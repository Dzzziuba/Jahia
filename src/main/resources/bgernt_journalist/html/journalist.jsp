<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>

<c:set var="Title" value="${currentNode.properties['Title'].string}"/>
<c:set var="Academic_title" value="${currentNode.properties['Academic_title'].string}"/>
<c:set var="Name" value="${currentNode.properties['Name'].string}"/>
<c:set var="Surname" value="${currentNode.properties['Surname'].string}"/>
<c:set var="Address" value="${currentNode.properties['Address'].string}"/>
<c:set var="NPA" value="${currentNode.properties['NPA'].long}"/>
<c:set var="Place" value="${currentNode.properties['Place'].string}"/>
<c:set var="Phone" value="${currentNode.properties['Phone'].string}"/>
<c:set var="Cellphone" value="${currentNode.properties['Cellphone'].string}"/>
<c:set var="Email" value="${currentNode.properties['Email'].string}"/>
<c:set var="Email_additional" value="${currentNode.properties['Email_additional']}"/>
<c:set var="Newspapers" value="${currentNode.properties['Newspapers'].string}"/>
<c:set var="Languages" value="${currentNode.properties['Languages']}"/>
<c:set var="Password" value="${currentNode.properties['Password'].string}"/>
<c:set var="Identity" value="${currentNode.properties['Identity'].string}"/>
<c:set var="Enable" value="${currentNode.properties['Enable'].boolean}"/>
<tr>
    <td>${Name}</td>
    <td>${Surname}</td>
    <td>${Email}</td>
    <td>
        <c:forEach items="${Email_additional}" var="item">
            <p style="padding: 2px">${item.string}</p>
        </c:forEach>
    </td>
    <td>
        <c:forEach items="${Languages}" var="item">
            <p style="padding: 2px">${item.string}</p>
        </c:forEach>
    </td>
</tr>


