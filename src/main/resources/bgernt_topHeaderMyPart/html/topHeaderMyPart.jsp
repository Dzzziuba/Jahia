<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>

<jcr:node var="languageSwitcherMyPart" path="${currentNode.path}/languageSwitcherMyPart"/>
<jcr:node var="topMenu" path="${currentNode.path}/topMenu"/>

<div class="unit size2of3">
    <div class="mod modNavService">
        <div class="inner">
            <div class="nav">
                <template:module path="${currentNode.path}/topMenu" view="top" />
                <template:module path="${currentNode.path}/languageSwitcherMyPart" />
            </div>
        </div>
    </div>
</div>
<div class="unit size1of3 lastUnit">
    <div class="mod modSearch">
        <div class="inner">
            <%--<template:module path="${currentNode.path}/searchBar" />--%>
        </div>
    </div>
</div>