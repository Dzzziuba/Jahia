<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="query" uri="http://www.jahia.org/tags/queryLib" %>
<%@ taglib prefix="ui" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>

<jcr:sql var="journalists" sql="select * from [bgernt:journalist] as j order by j.['Name']"/>
<jcr:sql limit="1" var="paginator"
         sql="SELECT * FROM [jnt:pager] as p where p.['j:bindedComponent']='${currentNode.identifier}'"/>

<c:set var="beginProperty" value="begin${currentNode.identifier}"/>
<c:set var="endProperty" value="end${currentNode.identifier}"/>
<c:set var="pageSizeProperty" value="pagesize${currentNode.identifier}"/>

<c:forEach items="${paginator.nodes}" var="realNode">
    <c:set var="pageSize"
           value="${empty param[pageSizeProperty] ? realNode.properties.pageSize.long : param[pageSizeProperty]}"/>
</c:forEach>

<c:set target="${moduleMap}" property="listTotalSize" value="${fn:length(journalists.nodes)}"/>

<c:set var="begin" value="${empty param[beginProperty] ? 0 : param[beginProperty]}"/>
<c:set var="end" value="${empty param[endProperty] ? param[beginProperty]+pageSize-1 : param[endProperty]}"/>
<style>
    table, th, td {
        border: 1px solid black;
    }

    th, td {
        padding: 15px;
        text-align: left;
    }
</style>
<table>
    <tr>
        <th style="width: 20%;"><fmt:message key="bgernt_journalist.Name"/> :</th>
        <th style="width: 20%"><fmt:message key="bgernt_journalist.Surname"/> :</th>
        <th style="width: 20%"><fmt:message key="bgernt_journalist.Email"/> :</th>
        <th style="width: 20%"><fmt:message key="bgernt_journalist.Email_additional"/> :</th>
        <th style="width: 20%"><fmt:message key="bgernt_journalist.Languages"/> :</th>
    </tr>

    <c:forEach items="${journalists.nodes}" var="node" begin="${begin}"
               end="${end lt 0 ? 0 : end}">
        <template:module node="${node}"/>
    </c:forEach>
</table>