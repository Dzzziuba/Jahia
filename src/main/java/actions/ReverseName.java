package actions;


import org.jahia.bin.Action;
import org.jahia.bin.ActionResult;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.render.RenderContext;
import org.jahia.services.render.Resource;
import org.jahia.services.render.URLResolver;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Get the root under the homepage for a page.
 * <p>
 * Example :
 * Home
 * - Menu Root 1
 * - Page 11
 * - Page 12
 * - Page 121
 * - Menu Root 2
 * -Page 21
 * - Page 211
 * <p>
 * For node "Page 121", the result is "Menu Root 1"
 * For node "Page 11", the result is "Menu Root 1"
 * For node "Page 21", the result is "Menu Root 2"
 *
 * @author Roman Dziuba <rodzi@smile.fr>
 */
public class ReverseName extends Action {
    /**
     * hello
     */
    @Override
    public ActionResult doExecute(final HttpServletRequest request, final RenderContext renderContext,
                                  final Resource resource,
                                  final JCRSessionWrapper jcrSessionWrapper, final Map<String, List<String>> map,
                                  final URLResolver urlResolver) throws java.lang.Exception {
        final int successResultCode = 200;
        final QueryManager jcrQueryManager = jcrSessionWrapper.getWorkspace().getQueryManager();
        final Query query = jcrQueryManager.createQuery("select * from [jnt:judge]", "JCR-SQL2");
        final NodeIterator nodeIterator = query.execute().getNodes();
        while (nodeIterator.hasNext()) {
            final Node currentNode = nodeIterator.nextNode();
            final String name = currentNode.getProperty("Name").getString();
            final String surname = currentNode.getProperty("Surname").getString();
            currentNode.setProperty("Name", surname);
            currentNode.setProperty("Surname", name);
            currentNode.refresh(true);
        }
        jcrSessionWrapper.save();
        return new ActionResult(successResultCode, renderContext.getMainResource().getNode().getPath());
    }
}
