package listeners;

import actions.SendMail;
import ch.bger.jahia.rules.Journalist;
import org.jahia.services.content.DefaultEventListener;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRSessionWrapper;
import org.slf4j.Logger;
import util.JahiaUtil;

import javax.jcr.RepositoryException;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;

/**
 * @author Roman Dziuba <rodzi@smile.fr>
 */
public class JournalistListner extends DefaultEventListener {

    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(Journalist.class);


    /**
     * The variable sendMail
     */
    private SendMail sendMail;

    /**
     * @return
     */
    @Override
    public int getEventTypes() {
        return Event.PROPERTY_CHANGED + Event.PROPERTY_ADDED + Event.PROPERTY_REMOVED;
    }

    //    /**
    //     * @return
    //     */
    //    @Override
    //    public String[] getNodeTypes() {
    //        return new String[]{"Journalist"};
    //   }


    /**
     * @param events
     */
    @Override
    public void onEvent(final EventIterator events) {
        try {
            final JCRSessionWrapper session = JahiaUtil.getCurrenUserSession();
            String propertyName;
            String path;
            String message;
            while (events.hasNext()) {
                final Event nextEvent = events.nextEvent();

                path = nextEvent.getPath();
                propertyName = path.substring(path.lastIndexOf('/') + 1, path.length());
                final JCRNodeWrapper nodeWrapper = session.getNodeByUUID(nextEvent.getIdentifier());
                if (nodeWrapper != null) {
                    switch (propertyName) {
                        case "Email_additional":
                            final String newAdditionalEmail = nodeWrapper.getPropertyAsString("Email_additional");
                            if (newAdditionalEmail.length() > 0) {
                                message = "Your additional mail was changed to : ";
                            } else {
                                message = "Your additional mail was removed";
                            }
                            sendMail.sendCustomMail(nodeWrapper.getPropertyAsString("Email"),
                                    message + newAdditionalEmail);
                            break;
                        case "Email":
                            final String newEmail = nodeWrapper.getPropertyAsString("Email");
                            message = "Your mail was changed to : ";
                            sendMail.sendCustomMail(newEmail,
                                    message + newEmail);
                            break;
                        default:
                            break;
                    }
                }
            }
        } catch (RepositoryException e) {
            LOGGER.error("onEvent: ", e);
        }

    }

    /**
     * @param sendMail
     */
    public void setSendMail(final SendMail sendMail) {
        this.sendMail = sendMail;
    }

}
